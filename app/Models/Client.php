<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
      "cni",
      "nom",
      "prenom",
      "date_naissance",
      "profession",
      "telephone",
      "photo",
      "signature",
      "sexe_id",
    ];

    public function sexe()
    {
      return $this->belongsTo(Sexe::class);
    }

    public function getNomCompletAttribute()
    {
      return ucwords($this->prenom.' '.strtoupper($this->nom));
    }

    public function getPhotoPathAttribute()
    {
      return asset(empty($this->photo) ? '/assets/media/users/blank.png' : str_replace('public/', '', 'storage/'.$this->photo));
    }

    public function comptes()
    {
      return $this->hasMany(Compte::class, 'client_id');
    }

    public function getSoldeAttribute()
    {
      $solde = 0;
      foreach ($this->comptes as $cp) {
        $solde += $cp->solde;
      }

      return $solde;
    }

    public function user()
    {
      return $this->hasOne(User::class, 'client_id');
    }
}
