<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $fillable = [
      "numero",
      "date",
      "montant",
      "visa",
      "observation",
      "agence_id",
      "mode_operation_id",
      "type_operation_id",
      "compte_id",
      "compte_dest_id",
    ];


    protected $casts = [
      "date" => "datetime",
      "montant" => "double",
    ];


    public function agence()
    {
      return $this->belongsTo(Agence::class);
    }

    public function mode()
    {
      return $this->belongsTo(ModeOperation::class, 'mode_operation_id');
    }

    public function type()
    {
      return $this->belongsTo(TypeOperation::class, 'type_operation_id');
    }

    public function compte()
    {
      return $this->belongsTo(Compte::class);
    }

    public function compte_dest()
    {
      return $this->belongsTo(Compte::class);
    }
}
