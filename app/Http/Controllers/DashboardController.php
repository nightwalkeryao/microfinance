<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Compte;
use App\Models\Operation;

class DashboardController extends Controller
{
  public function home()
  {
    $user = auth()->user();
    $vars = [];
    $vars["user"] = $user;

    if($user->role == 'client') {
      $solde = 0;
      $comptes = Compte::where('client_id', $user->client_id)->get();
      foreach ($comptes as $c) {
        $solde += $c->solde;
      }
      $vars['solde'] = $solde;

      $total_depot = 0;
      foreach (Operation::whereIn('compte_id', $comptes->pluck('id'))->where('type_operation_id', 1)->get() as $op) {
        $total_depot += $op->montant;
      }
      $vars['total_depot'] = $total_depot;

      $total_retrait = 0;
      foreach (Operation::whereIn('compte_id', $comptes->pluck('id'))->where('type_operation_id', 2)->get() as $op) {
        $total_retrait += $op->montant;
      }
      $vars['total_retrait'] = $total_retrait;
    }

    return view("contents.dashboards.".auth()->user()->role, $vars);
  }
}
