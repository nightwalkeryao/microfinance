<?php

namespace App\Http\Controllers;

use App\Models\Agence;
use Illuminate\Http\Request;
use App\Http\Requests\AgenceCreateRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use Carbon\Carbon;

class AgenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
        $model = Agence::query();
        return FacadesDataTables::eloquent($model)
        ->filter(function ($query) {
          if (request()->has('query.q')) {
            $query->where('libelle', 'like', "%" . request('query.q') . "%")
            ->orWhere('localisation', 'like', "%" . request('query.q') . "%");
          }
          if(request()->has('sort.field') && request()->has('sort.sort') && in_array(request('sort.field'), ["DT_RowIndex", "libelle", "localisation", "contact"]) && in_array(request('sort.sort'), ['asc', 'desc'])) {
            $cols = [
              "DT_RowIndex" => "id",
              "libelle" => "libelle",
              "localisation" => "localisation",
              "contact" => "contact",
            ];
            $query->orderBy($cols[request("sort.field")], request("sort.sort"));
          } else {
            $query->orderByDesc('id');
          }
        }, true)
        ->addIndexColumn()
        ->editColumn('created_at', function (Agence $a) {
          return Carbon::parse($a->created_at)->locale('fr')->isoFormat("DD MMM YYYY, HH:mm");
        })
        ->addColumn('Actions', function ($row) {
          return 'Edit/Delete';
        })
        ->toJson();
      }

        return view("contents.agences.list", [
          'count' => Agence::count(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contents.agences.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgenceCreateRequest $request)
    {
      $input = $request->validated();
      Agence::create($input);

      return response()->json([
        'success' => true,
        'message' => "L'Agence a bien été ajoutée.",
        // 'input' => $input,
        'redirect' => route('list-agences'),
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function show(Agence $agence)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function edit(Agence $agence)
    {
        return view('contents.agences.edit', [
          'agence' => $agence,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function update(AgenceCreateRequest $request, Agence $agence)
    {
      $input = $request->validated();
      $continue = $input['continue'] ?? route('show-agence', ['agence' => $agence->id]);
      unset($input['continue']);
      $agence->update($input);

      return response()->json([
        'success' => true,
        'message' => "L'Agence a bien été mise à jour.",
        // 'input' => $input,
        'redirect' => $continue,
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agence $agence)
    {
      $agence->delete();
      return redirect()->route('list-agences');
    }
}
