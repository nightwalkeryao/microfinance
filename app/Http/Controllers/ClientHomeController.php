<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Compte;
use App\Models\Operation;
use App\Models\Agence;
use App\Models\TypeOperation;
use App\Models\ModeOperation;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use Carbon\Carbon;

class ClientHomeController extends Controller
{
  public function operations(Request $request)
  {
    $user = auth()->user();
    $comptes = Compte::where('client_id', $user->client_id)->get();

    if ($request->ajax()) {
      $model = Operation::whereIn('compte_id', $comptes->pluck('id'));
      return FacadesDataTables::eloquent($model->with(['agence', 'compte', 'compte.client', 'type', 'mode', 'compte_dest']))
      ->filter(function ($query) {
        if (request()->has('query.q')) {
          $query->where('numero', 'like', "%" . request('query.q') . "%");
        }
        if(request()->has('query.agence') && request("query.agence") != "all") {
          $query->where('agence_id', request('query.agence'));
        }
        if(request()->has('query.type') && request("query.type") != "all") {
          $query->where('type_operation_id', request('query.type'));
        }
        if(request()->has('query.mode') && request("query.mode") != "all") {
          $query->where('mode_operation_id', request('query.mode'));
        }
        if(request()->has('sort.field') && request()->has('sort.sort') && in_array(request('sort.field'), ["DT_RowIndex", "numero", "date", "montant", "agence", "type_operation", "compte", "created_at"]) && in_array(request('sort.sort'), ['asc', 'desc'])) {
          $cols = [
            "DT_RowIndex" => "id",
            "numero" => "numero",
            "date" => "date",
            "montant" => "montant",
            "agence" => "agence_id",
            "type_operation" => "type_operation_id",
            "compte" => "compte_id",
            "created_at" => "created_at",
          ];
          $query->orderBy($cols[request("sort.field")], request("sort.sort"));
        } else {
          $query->orderByDesc('id');
        }
      }, true)
      ->addIndexColumn()
      ->editColumn('photo', function (Operation $o) {
        return $o->compte->client->photo_path;
      })
      ->editColumn('created_at', function (Operation $o) {
        return Carbon::parse($o->date)->locale('fr')->isoFormat("DD MMM YYYY, HH:mm");
      })
      ->addColumn('nom_complet', function (Operation $o) {
        return $o->compte->client->nom_complet;
      })
      ->addColumn('action', function ($row) {
        return 'Edit/Delete';
      })
      ->toJson();
    }

    return view("contents.operations.list-operations-client", [
      'agences' => Agence::all(),
      'modes_operation' => ModeOperation::all(),
      'types_operation' => TypeOperation::all(),
      'count' => Operation::whereIn('compte_id', $comptes->pluck('id'))->count(),
    ]);
  }


  public function comptes(Request $request)
  {
    if ($request->ajax()) {
      $model = Compte::where('client_id', auth()->user()->client_id);
      return FacadesDataTables::eloquent($model->with(['client']))
      ->filter(function ($query) {
        if (request()->has('query.q')) {
          $query->where('numero', 'like', "%" . request('query.q') . "%");
        }
        if(request()->has('sort.field') && request()->has('sort.sort') && in_array(request('sort.field'), ["DT_RowIndex", "numero", "date_ouverture", "visa_ouverture", "date_fermeture", "visa_fermeture", "solde", "client", "created_at"]) && in_array(request('sort.sort'), ['asc', 'desc'])) {
          $cols = [
            "DT_RowIndex" => "id",
            "numero" => "numero",
            "date_ouverture" => "date_ouverture",
            "visa_ouverture" => "visa_ouverture",
            "date_fermeture" => "date_fermeture",
            "visa_fermeture" => "visa_fermeture",
            "solde" => "solde",
            "client" => "client_id",
            "created_at" => "created_at",
          ];
          $query->orderBy($cols[request("sort.field")], request("sort.sort"));
        } else {
          $query->orderByDesc('id');
        }
      }, true)
      ->addIndexColumn()
      ->editColumn('created_at', function (Compte $c) {
        return Carbon::parse($c->created_at)->locale('fr')->isoFormat("DD MMM YYYY, HH:mm");
      })
      ->editColumn('date_ouverture', function (Compte $c) {
        return Carbon::parse($c->date_ouverture)->locale('fr')->isoFormat("DD/MM/YYYY");
      })
      ->addColumn('action', function ($row) {
        return 'Edit/Delete';
      })
      ->toJson();
    }

    return view("contents.comptes.list-comptes-client", [
      'count' => Compte::where('client_id', auth()->user()->client_id)->count(),
    ]);
  }
}
