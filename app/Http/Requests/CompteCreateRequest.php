<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompteCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "numero" => "required|string|between:5,11|unique:comptes,numero",
            "date_ouverture" => "required|date|date_format:Y-m-d",
            "observation" => "nullable|string|max:500",
            "solde" => "nullable|numeric|min:0",
            "client_id" => "required|integer|exists:clients,id",
        ];
    }
}
