<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgenceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "libelle" => "required|string|between:2,160",
          "localisation" => "required|string|between:2,250",
          "contact" => "required|string|between:2,160",
          "continue" => "sometimes|nullable|url",
        ];
    }
}
