<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RetraitMakeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "numero" => "required|string|between:5,15|unique:operations,numero",
          "montant" => "required|numeric|min:1",
          "observation" => "nullable|string|max:255",
          "agence_id" => "required|integer|exists:agences,id",
          "compte_id" => "required|integer|exists:comptes,id",
        ];
    }
}
