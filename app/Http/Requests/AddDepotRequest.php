<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddDepotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'cni' => "required|string|between:5,15",
            'nom' => "required|string|between:2,50",
            'prenom'=>"required|string|between:2,50",
            "numero"=>"required|string|between:2,50",
            "montant"=>"required|string",

        ];
    }
    public function messages()
    {
        return [
            'cni.required' => "Le numéro de la CNI est requis.",
            'cni.between' => "Le numéro de la CNI doit faire entre :min et :max caractères.",
            'nom.required' => "Le nom est requis.",
            'nom.between' => "Le nom doit faire entre :min et :max caractères.",
            'prenom.required' => "Le prénom est requis.",
            'prenom.between' => "Le prénom doit faire entre :min et :max caractères.",
            'numero.required' => "Le numero est requis.",
            'numero.between' => "Le numero doit faire entre :min et :max caractères.",
            'montant.required' => "Le montant est requis.",
            
          ];
    }
}
