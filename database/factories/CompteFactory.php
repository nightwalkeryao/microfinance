<?php

namespace Database\Factories;

use App\Models\Compte;
use App\Models\User;
use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Compte::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $max = Client::count();
        return [
          "numero" => $this->faker->swiftBicNumber(),
          "date_ouverture" => $this->faker->date(),
          "visa_ouverture" => User::where('role', 'gerant_adjoint')->first()->name,
          "date_fermeture" => null,
          "visa_fermeture" => null,
          "observation" => $this->faker->realText(200),
          "solde" => $this->faker->randomNumber(2)*1000,
          "client_id" => mt_rand(1, $max),
        ];
    }
}
