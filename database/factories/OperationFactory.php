<?php

namespace Database\Factories;

use App\Models\Operation;
use App\Models\User;
use App\Models\Agence;
use App\Models\Compte;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Operation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      $ag_max = Agence::count();
      $cp_max = Compte::count();
        return [
          "numero" => "01".$this->faker->randomNumber(7),
          "date" => $this->faker->date(),
          "montant" => $this->faker->randomNumber(2) * 1000,
          "visa" => User::where('role', 'caissiere')->first()->name,
          "observation" => $this->faker->realText(200),
          "agence_id" => mt_rand(1, $ag_max),
          "mode_operation_id" => mt_rand(1, 3),
          "type_operation_id" => mt_rand(1, 2),
          "compte_id" => mt_rand(1, $cp_max),
          "compte_dest_id" => null,
        ];
    }
}
