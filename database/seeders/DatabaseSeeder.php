<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(SexesSeeder::class);
        $this->call(TypesOperationSeeder::class);
        $this->call(ModesOperationSeeder::class);
        $this->call(UsersSeeder::class);
        \App\Models\Agence::factory(100)->create();
        \App\Models\Client::factory(50)->create();
        \App\Models\Compte::factory(200)->create();
        $this->call(ClientUserSeeder::class);
        \App\Models\Operation::factory(5000)->create();
    }
}
