@extends('layouts.dashboard')

@section('title')
  Agences
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Liste des Agences</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-agences') }}" class="text-muted">Agences</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md mr-1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              Retour
            </a>
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b">
          <!--begin::Header-->
          <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label font-weight-bolder text-dark">Agences</span>
              <span class="text-muted mt-3 font-weight-bold font-size-sm">Liste des {{ $count }} Agences</span>
            </h3>
            <div class="card-toolbar">
              <a href="{{ route('create-agence') }}" class="btn btn-success font-weight-bolder font-size-sm">
                <span class="svg-icon svg-icon-md svg-icon-white">
                  <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo8\dist/../src/media/svg/icons\Shopping\Wallet3.svg-->
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <rect x="0" y="0" width="24" height="24"/>
                      <path d="M4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 Z" fill="#000000" opacity="0.3"/>
                      <path d="M18.5,11 L5.5,11 C4.67157288,11 4,11.6715729 4,12.5 L4,13 L8.58578644,13 C8.85100293,13 9.10535684,13.1053568 9.29289322,13.2928932 L10.2928932,14.2928932 C10.7456461,14.7456461 11.3597108,15 12,15 C12.6402892,15 13.2543539,14.7456461 13.7071068,14.2928932 L14.7071068,13.2928932 C14.8946432,13.1053568 15.1489971,13 15.4142136,13 L20,13 L20,12.5 C20,11.6715729 19.3284271,11 18.5,11 Z" fill="#000000"/>
                      <path d="M5.5,6 C4.67157288,6 4,6.67157288 4,7.5 L4,8 L20,8 L20,7.5 C20,6.67157288 19.3284271,6 18.5,6 L5.5,6 Z" fill="#000000"/>
                    </g>
                  </svg>
                  <!--end::Svg Icon-->
                </span>Ouvrir une Agence</a>
              </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body">

              <div class="mb-7">
							<div class="row align-items-center">
								<div class="col-lg-9 col-xl-8">
									<div class="row align-items-center">
										<div class="col-md-12 my-2 my-md-0">
											<div class="input-icon">
												<input type="text" class="form-control" placeholder="Chercher..." id="kt_datatable_search_query" />
												<span>
													<i class="flaticon2-search-1 text-muted"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
									<a class="btn btn-light-primary px-6 font-weight-bold" id="kt_search_trigger">Chercher</a>
								</div>
							</div>
						</div>

              <!--begin: Datatable-->
              <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
              <!--end: Datatable-->

            </div>
            <!--end::Body-->
          </div>
        </div>
        <!--end::Content-->
      </div>
      <!--end::Content Wrapper-->
    </div>
  @endsection

  @section('specific-js')
    <script type="text/javascript">
    "use strict";
    // Class definition

    var KTDatatableRemoteAjax = function() {
      // Private functions

      // basic demo
      var demo = function() {

        var datatable = $('#kt_datatable').KTDatatable({
          // datasource definition
          data: {
            type: 'remote',
            source: {
              read: {
                method: 'get',
                url: window.location.href,
                // sample custom headers
                // headers: {'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState: false
          },

          // layout definition
          layout: {
            scroll: false,
            footer: false,
            spinner: {
              message: "Chargement des données..."
            }
          },

          // column sorting
          sortable: true,

          pagination: true,

          search: {
            input: $('#kt_datatable_search_query'),
            key: 'q',
            delay: 400
          },

          translate: {
            records: {
              processing: "Chargement des données...",
              noRecords: "Il n'y a rien à afficher."
            },
            toolbar: {
              pagination: {
                items: {
                  default: {
                    first: "Début",
                    prec: "Précédent",
                    next: "Suivant",
                    last: "Fin",
                    more: "Plus",
                    input: "Numéro de page",
                    select: "Sélectionner la page"
                  },
                  info: "{{ str_replace(["(", ")"], ["{", "}"], 'Agences ((start)) - ((end)) sur ((total))') }}"
                }
              }
            }
          },

          // columns definition
          columns: [{
            field: 'DT_RowIndex',
            title: '#',
            sortable: 'asc',
            width: 48,
            type: 'number',
            selector: false,
            textAlign: 'center',
            template: function(row) {
              return `<span class="text-dark-75 font-weight-bolder d-block font-size-lg">`+row.DT_RowIndex+`</span>`;
            }
          }, {
            field: 'libelle',
            title: 'Libellé',
            width: 250,
            template: function(row) {
              let output = `
              <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{ route('show-agence', ['agence' => '__id__']) }}">`+row.libelle+`</a></span>
              `;
              return output.replace(/__id__/g, row.id);
            }
          }, {
            field: 'localisation',
            title: 'Localisation',
            width: 300,
            template: function(row) {
              let tpl = `<span class="text-muted d-block font-size-lg">`+row.localisation+`</span>`;
              return tpl;
            }
          }, {
            field: 'contact',
            title: 'Contact',
            width: 120,
            template: function(row) {
              return `<span class="text-muted d-block font-size-lg">`+row.contact+`</span>`;
            }
          }, {
            field: 'created_at',
            title: 'Date ouv.',
            width: 120,
            type: 'date',
            format: 'DD/MM/YYYY',
            template: function(row) {
              return `<span class="text-muted d-block font-size-lg">`+row.created_at+`</span>`
            }
          }, {
            field: 'Actions',
            title: 'Actions',
            sortable: false,
            width: 150,
            textAlign: 'right',
            overflow: 'visible',
            autoHide: false,
            template: function(row) {
              let tpl = `
              <a href="{{ route('show-agence', ['agence' => '__id__']) }}" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-md svg-icon-primary">
                  <!--begin::Svg Icon | path:assets/media/svg/icons/General/Settings-1.svg-->
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <rect x="0" y="0" width="24" height="24"></rect>
                      <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
                      <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
                    </g>
                  </svg>
                  <!--end::Svg Icon-->
                </span>
              </a>
              <a href="{{ route('edit-agence', ['agence' => '__id__']) }}" class="btn btn-icon btn-light btn-hover-primary btn-sm mx-3">
                <span class="svg-icon svg-icon-md svg-icon-primary">
                  <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Write.svg-->
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <rect x="0" y="0" width="24" height="24"></rect>
                      <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
                      <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                    </g>
                  </svg>
                  <!--end::Svg Icon-->
                </span>
              </a>
              <a onClick="$(this).hiddenPost();" data-href="{{ route('delete-agence', ['agence' => '__id__']) }}" class="confirm btn btn-icon btn-light btn-hover-primary btn-sm" data-confirm="Souhaitez-vous vraiment retirer cette Agence ?">
                <span class="svg-icon svg-icon-md svg-icon-primary">
                  <!--begin::Svg Icon | path:assets/media/svg/icons/General/Trash.svg-->
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <rect x="0" y="0" width="24" height="24"></rect>
                      <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                      <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                    </g>
                  </svg>
                  <!--end::Svg Icon-->
                </span>
              </a>
              `
              return tpl.replace(/__id__/g, row.id);
            }
          }],

        });

        $('#kt_search_trigger').click(function(e){
          e.preventDefault();
          datatable.setDataSourceParam("query[q]", $("#kt_datatable_search_query").val());
          datatable.load();
        });
      };

      return {
        // public functions
        init: function() {
          demo();
        },
      };
    }();

    jQuery(document).ready(function() {
      KTDatatableRemoteAjax.init();
    });
  </script>
@endsection
