@extends('layouts.dashboard')

@section('title')
  Mes Comptes
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Liste des Comptes</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-comptes-client') }}" class="text-muted">Mes Comptes</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md mr-1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              Retour
            </a>
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b">
          <!--begin::Header-->
          <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label font-weight-bolder text-dark">Mes Comptes</span>
              <span class="text-muted mt-3 font-weight-bold font-size-sm">Liste des {{ $count }} Comptes</span>
            </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body">

              <div class="mb-7">
							<div class="row align-items-center">
								<div class="col-lg-9 col-xl-8">
									<div class="row align-items-center">
										<div class="col-md-12 my-2 my-md-0">
											<div class="input-icon">
												<input type="text" class="form-control" placeholder="Chercher..." id="kt_datatable_search_query" />
												<span>
													<i class="flaticon2-search-1 text-muted"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
									<a class="btn btn-light-primary px-6 font-weight-bold" id="kt_search_trigger">Chercher</a>
								</div>
							</div>
						</div>

              <!--begin: Datatable-->
              <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
              <!--end: Datatable-->

            </div>
            <!--end::Body-->
          </div>
        </div>
        <!--end::Content-->
      </div>
      <!--end::Content Wrapper-->
    </div>
  @endsection

  @section('specific-js')
    <script type="text/javascript">
    "use strict";
    // Class definition

    var KTDatatableRemoteAjax = function() {
      // Private functions

      // basic demo
      var demo = function() {

        var datatable = $('#kt_datatable').KTDatatable({
          // datasource definition
          data: {
            type: 'remote',
            source: {
              read: {
                method: 'get',
                url: window.location.href,
                // sample custom headers
                // headers: {'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState: false
          },

          // layout definition
          layout: {
            scroll: false,
            footer: false,
            spinner: {
              message: "Chargement des données..."
            }
          },

          // column sorting
          sortable: true,

          pagination: true,

          search: {
            input: $('#kt_datatable_search_query'),
            key: 'q',
            delay: 400
          },

          translate: {
            records: {
              processing: "Chargement des données...",
              noRecords: "Il n'y a rien à afficher."
            },
            toolbar: {
              pagination: {
                items: {
                  default: {
                    first: "Début",
                    prec: "Précédent",
                    next: "Suivant",
                    last: "Fin",
                    more: "Plus",
                    input: "Numéro de page",
                    select: "Sélectionner la page"
                  },
                  info: "{{ str_replace(["(", ")"], ["{", "}"], 'Comptes ((start)) - ((end)) sur ((total))') }}"
                }
              }
            }
          },

          // columns definition
          columns: [{
            field: 'DT_RowIndex',
            title: '#',
            sortable: 'asc',
            width: 40,
            type: 'number',
            selector: false,
            textAlign: 'center',
            template: function(row) {
              return `<span class="text-dark-75 font-weight-bolder d-block font-size-lg">`+row.DT_RowIndex+`</span>`;
            }
          }, {
            field: 'client',
            title: 'Client',
            width: 300,
            template: function(row) {
              let output = `
              <div class="d-flex align-items-center">
                <div class="symbol symbol-50 flex-shrink-0 mr-4">
                  <div class="symbol-label" style="background-image: url(/storage/`+row.client.photo+`)"></div>
                </div>
                <div>
                  <a href="{{ route("show-client", ['client' => "__id__"]) }}" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">`+row.client.prenom+` `+row.client.nom.toUpperCase()+`</a>
                  <span class="text-muted font-weight-bold d-block">`+ row.client.profession +`</span>
                </div>
              </div>
              `;
              return output.replace(/__id__/g, row.id);
            }
          }, {
            field: 'numero',
            title: 'Numéro',
            width: 150,
            template: function(row) {
              let tpl = `<span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{ route('show-compte', ['compte' => '__id__']) }}">`+row.numero+`</a></span>`;
              return tpl.replace(/__id__/g, row.id);
            }
          }, {
            field: 'solde',
            title: 'Solde',
            width: 90,
            template: function(row) {
              return `<span class="text-dark-75 font-weight-bolder d-block font-size-lg">`+row.solde+` FCFA</span>`;
            }
          }, {
            field: 'date_ouverture',
            title: 'Date ouv.',
            width: 100,
            type: 'date',
            format: 'DD/MM/YYYY',
            template: function(row) {
              return `<span class="text-muted font-weight-bolder d-block font-size-lg">`+row.date_ouverture+`</span>`
            }
          }, {
            field: 'visa_ouverture',
            title: 'Visa ouv.',
            width: 150,
            template: function(row) {
              return `<span class="text-muted font-weight-bolder d-block font-size-lg">`+row.visa_ouverture+`</span>`;
            }
          }/*, {
            field: 'Actions',
            title: 'Actions',
            sortable: false,
            width: 150,
            textAlign: 'right',
            overflow: 'visible',
            autoHide: false,
            template: function(row) {
              let tpl = `
              <a href="{{ route('show-compte', ['compte' => '__id__']) }}" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-md svg-icon-primary">
                  <!--begin::Svg Icon | path:assets/media/svg/icons/General/Settings-1.svg-->
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <rect x="0" y="0" width="24" height="24"></rect>
                      <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
                      <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
                    </g>
                  </svg>
                  <!--end::Svg Icon-->
                </span>
              </a>
              `
              return tpl.replace(/__id__/g, row.id);
            }
          }*/],

        });

        $('#kt_search_trigger').click(function(e){
          e.preventDefault();
          datatable.setDataSourceParam("query[q]", $("#kt_datatable_search_query").val());
          datatable.load();
        });
      };

      return {
        // public functions
        init: function() {
          demo();
        },
      };
    }();

    jQuery(document).ready(function() {
      KTDatatableRemoteAjax.init();
    });
  </script>
@endsection
