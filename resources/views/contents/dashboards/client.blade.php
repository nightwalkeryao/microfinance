@extends('layouts.dashboard')

@section('title')
  Tableau de bord
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">{{ auth()->user()->name }}</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="" class="text-muted">Mon Compte</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <!--begin::Daterange-->
            <a class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="kt_dashboard_daterangepicker">
              <span class="opacity-60 font-weight-bold mr-2">Aujourd'hui:</span>
              <span class="font-weight-bold">{{ Carbon\Carbon::parse(date("Y-m-d"))->locale('fr')->isoFormat("DD MMMM Y") }}</span>
            </a>
            <!--end::Daterange-->
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
          <div class="card-body">
            <!--begin::Details-->
            <div class="d-flex mb-9">
              <!--begin: Pic-->
              <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                <div class="symbol symbol-50 symbol-lg-120">
                  <img src="{{ auth()->user()->photo_path }}" alt="image">
                </div>
                <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                  <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                </div>
              </div>
              <!--end::Pic-->
              <!--begin::Info-->
              <div class="flex-grow-1">
                <!--begin::Title-->
                <div class="d-flex justify-content-between flex-wrap mt-1">
                  <div class="d-flex mr-3">
                    <a class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{ auth()->user()->name }}</a>
                    <a>
                      @if(auth()->user()->active)
                        <i class="flaticon2-correct text-success font-size-h5"></i>
                      @endif
                    </a>
                  </div>
                </div>
                <!--end::Title-->
                <!--begin::Content-->
                <div class="d-flex flex-wrap justify-content-between mt-1">
                  <div class="d-flex flex-column flex-grow-1 pr-8">
                    <div class="d-flex flex-wrap mb-4">
                      <a class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                        <i class="flaticon2-new-email mr-2 font-size-lg"></i>{{ auth()->user()->email }}
                      </a>
                      <a class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                        <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>{{ strtoupper(auth()->user()->role) }}
                      </a>
                    </div>
                    <span class="font-weight-bold text-dark-50">I distinguish three main text objectives could be merely to inform people.</span>
                    <span class="font-weight-bold text-dark-50">A second could be persuade people.You want people to bay objective</span>
                  </div>
                </div>
                <!--end::Content-->
              </div>
            </div>
            <!--end::Info-->
          </div>
          <!--end::Details-->
          <div class="separator separator-solid"></div>
          <!--begin::Items-->
          <div class="d-flex align-items-center flex-wrap mt-8 px-10 pb-5">
            <!--begin::Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
              <span class="mr-4">
                <i class="flaticon-piggy-bank display-4 text-muted font-weight-bold"></i>
              </span>
              <div class="d-flex flex-column text-dark-75">
                <span class="font-weight-bolder font-size-sm">Dépôts</span>
                <span class="font-weight-bolder font-size-h5">
                  {{ $total_depot }} <span class="text-dark-50 font-weight-bold">FCFA</span>
                </span>
              </div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
              <span class="mr-4">
                <i class="flaticon-confetti display-4 text-muted font-weight-bold"></i>
              </span>
              <div class="d-flex flex-column text-dark-75">
                <span class="font-weight-bolder font-size-sm">Retraits</span>
                <span class="font-weight-bolder font-size-h5">
                  {{ $total_retrait }} <span class="text-dark-50 font-weight-bold">FCFA</span>
                </span>
              </div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
              <span class="mr-4">
                <i class="flaticon-pie-chart display-4 text-muted font-weight-bold"></i>
              </span>
              <div class="d-flex flex-column text-dark-75">
                <span class="font-weight-bolder font-size-sm">Solde</span>
                <span class="font-weight-bolder font-size-h5">
                  {{ $solde }} <span class="text-dark-50 font-weight-bold">FCFA</span>
                </span>
              </div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
              <span class="mr-4">
                <i class="flaticon-file-2 display-4 text-muted font-weight-bold"></i>
              </span>
              <div class="d-flex flex-column flex-lg-fill">
                <span class="text-dark-75 font-weight-bolder font-size-sm">Relévé de compte</span>
                <a href="{{ route('list-operations-client') }}" class="text-primary font-weight-bolder">Voir</a>
              </div>
            </div>
            <!--end::Item-->
          </div>
        </div>
      </div>
      <!--end::Card-->
    </div>
    <!--end::Content-->
  </div>
  <!--begin::Content Wrapper-->
@endsection
@section('specific-js')
  <script src="/assets/js/pages/widgets.js"></script>
@endsection
