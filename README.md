# Project : MicroFinance

###### Version : 2021.0-dev

|FOR DEVELOPMENT PURPOSES|
|:------|
|Architecture:  **Model(/app/Models) Vue(/resources/views) Controller(/app/Http/Controllers)**|
| Framework: **Laravel**|
| Framework version: **8.12**|
| Requirements manager: **Composer 2.0** |

## Configuration requise pour l'environnement de développement/production
- Apache 2.4+ ou Nginx 1.14+
- PHP 7.3+ (et PHP 7.3+-cli, développement seulement)
- Composer 2.0
- MariaDB 10.3+
- Git (Git Bash, si vous êtes sous Windows)
- Laravel 8.12+ (développement seulement)

## Installation
1. Cloner le dépôt dans votre dossier web : `git clone <url vers le dépôt> microfinance`
2. Aller dans le dossier `microfinance` : `cd microfinance`
3. Installer/mettre à jour les dépendances : `composer install` ou `composer update`
4. Créer une base de données depuis votre gestionnaire de base de données (_phpMyAdmin_ par exemple)
6. Modifier le fichier `.env` avec le nom de l'application, l'environnement (development/production), les identifiants de la base de données, les paramètres d'e-mail
5. Générer les fichiers de Configuration et la clé de l'environnement : `php artisan key:generate`
7. Migrer la base de données : `php artisan migrate --seed --force`
8. [Production] Mettre en cache la Configuration : `php artisan config:cache`
9. [Développement] Démarrer le serveur de développement : `php artisan serve`
10. Créer un symlink pour le storage `php artisan storage:link`

## Intégration des mises à jour
Pour prendre les changements depuis le dépôt : `git fetch && git pull`.
En cas de changements sur la structure de la base de données, faire `php artisan migrate --seed --force` pour intégrer seulement les changements sur la structure, `php artisan migrate:refresh --seed --force` pour réinstaller complètement la base de données.
